﻿using System.ComponentModel.DataAnnotations;
namespace EjercicioPropedeutico.Models
{
    public class ProveedorModel
    {
        public int Id { get; set; }
        [Display(Name = "Código")]
        public string Codigo { get; set; }

        [Display(Name = "Razón social")]
        [StringLength(150, MinimumLength = 13, ErrorMessage = "Debe ser entre 13 y 150 Caracteres")]
        public string RazonSocial { get; set; }

        [Display(Name = "RFC")]
        [StringLength(15, MinimumLength = 13, ErrorMessage = "Debe ser entre 13 y 15 Caracteres")]
        public string RFC { get; set; }
        public decimal Costo { get; set; }
        public string RazonSocialProveedor { get; set; }
    }



}