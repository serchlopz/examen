﻿using System.ComponentModel.DataAnnotations;
namespace EjercicioPropedeutico.Models
{
    public class ProductoModel1
    {
        public int Id { get; set; }
        [Display(Name = "Código del Proveedor:")]
        public int IdProveedor { get; set; }
        [Display(Name = "Codigo del Producto:")]
        [StringLength(150, MinimumLength = 13, ErrorMessage = "Debe ser entre 13 y 150 Caracteres")]
        public string Codigo { get; set; }
        [Display(Name = "Descripcion:")]
        [StringLength(400, MinimumLength = 13, ErrorMessage = "Debe ser entre 13 y 400 Caracteres")]
        public string Descripcion { get; set; }
        [Display(Name = "Unidad:")]
        [StringLength(3, MinimumLength = 1, ErrorMessage = "Debe ser entre 1 y 3 Caracteres")]
        public string Unidad { get; set; }
        [Display(Name = "Costo:")]
        [Range(1, maximum: 5000)]
        public decimal Costo { get; set; }
        public string RazonSocialProveedor { get; set; }
    }
}