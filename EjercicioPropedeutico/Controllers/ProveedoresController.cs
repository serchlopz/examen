﻿using EjercicioPropedeutico.Business;
using EjercicioPropedeutico.Etities;
using EjercicioPropedeutico.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace EjercicioPropedeutico.Controllers
{
    public class ProveedoresController : Controller
    {
        public ActionResult Index(string busqueda)
        {
            ProveedoresBusiness proveedoresBusiness = new ProveedoresBusiness();
            List<ProveedorModel> proveedores = new List<ProveedorModel>();
            var proveedoresBase = proveedoresBusiness.ObtenerTodos();
            proveedores = proveedoresBase.Select(p =>
                 new ProveedorModel()
                 {
                     Id = p.id_proveedor,
                     Codigo = p.codigo,
                     RazonSocialProveedor = p.razon_social,
                     RazonSocial = p.razon_social,
                     RFC = p.RFC
                 }).ToList();
            if (!string.IsNullOrWhiteSpace(busqueda))
            {
                proveedores = proveedores.Where(p => p.RazonSocialProveedor.Contains(busqueda) ||
                    p.Codigo.Contains(busqueda) ||
                    p.RazonSocialProveedor.Contains(busqueda)).ToList();
            }
            proveedores = proveedores.OrderBy(p => p.RazonSocialProveedor).ToList();
            return View(proveedores);
            //  return View(proveedores);
        }
        [HttpGet]
        public ActionResult Add()
        {
            ProveedoresBusiness proveedoresBusiness = new ProveedoresBusiness();
            ViewBag.Proveedores = proveedoresBusiness.ObtenerTodos();
            return View();
        }
        [HttpPost]
        public ActionResult Add(Models.ProveedorModel model)
        {
            ProveedoresBusiness proveedoresBusiness = new ProveedoresBusiness();
            ViewBag.Proveedores = proveedoresBusiness.ObtenerTodos();
            List<char> letrasPermitidas = new char[] { 'A', 'C', 'D', 'E', 'F' }.ToList();
            if (model == null)
            {
                throw new ArgumentNullException(nameof(model));
            }
            if (!letrasPermitidas.Contains(model.RFC.FirstOrDefault()))
            {
                ModelState.AddModelError("RFC", "Debe comenzar con la letra : A, C, D, E, F");
            }
            if (ModelState.IsValid)
            {

                //Etities.Proveedores proveedoresAgregar = new Etities.Proveedores()
                Proveedores proveedoresAgregar = new Proveedores()
                {
                    codigo = model.Codigo,
                    razon_social = model.RazonSocial,
                    RFC = model.RFC
                };
                new Business.ProveedoresBusiness().Agregar(proveedoresAgregar);
                ViewBag.Mensaje = "Se guardó correctamete";
                return View(new Models.ProveedorModel());
            }
            return View(model);
        }
        public ActionResult Edit(int Id)//comienza a editar
        {
            ProveedoresBusiness proveedoresBusiness = new ProveedoresBusiness();
            Proveedores proveedor = proveedoresBusiness.ObtenerPorId(Id);
            ProveedorModel model = new ProveedorModel()
            {
                Id = proveedor.id_proveedor,
                Codigo = proveedor.codigo,
                RazonSocial = proveedor.razon_social,
                RFC = proveedor.RFC
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(ProveedorModel model)
        {
            if (ModelState.IsValid)
            {
                Etities.Proveedores proveedorAEditar = new Etities.Proveedores()
                {
                    id_proveedor = model.Id,
                    codigo = model.Codigo,
                    razon_social = model.RazonSocial,
                    RFC = model.RFC
                };
                new ProveedoresBusiness().Edit(proveedorAEditar);
                TempData["Mensaje"] = "Se guardó correctamete";
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public JsonResult Remove(int Id,bool forzar = false)
        {
            ProveedoresBusiness proveedoresBusiness = new ProveedoresBusiness();
            try
            {
                proveedoresBusiness.EliminarPorId(Id,forzar);
                TempData["Mensaje"] = "Eliminado Satisfactoriamente";//aqui necesito agregar mi eliminacion del elemento
                return Json(new { exito = true }, JsonRequestBehavior.AllowGet);
            }
            catch (IdNegativoException negativoException)
            {
                TempData["Error"] = "El id es negativo";
            }
            catch (Exception e)
            {
                TempData["Error"] = "Excepcio desconocida";
            }
            ResultadoElimiacion resultado = new ResultadoElimiacion();
            resultado.exito = false;
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ValidarTieneProductos(int Id)
        {
            ProductoBusiness productoBusiness = new ProductoBusiness();
            try
            {
                List<Etities.Producto> productos = productoBusiness.ObtenerPorProveedor(Id);
                if(productos.Any())
                {
                    return Json(new { exito = true, tiene = true, numeroProductos=productos.Count});
                }
                else
                {
                    return Json(new { exito = true, tiene = false });
                }
            }
            catch
            {
                return Json(new { exito = false });
            }
        }
    }
    public class ResultadoElimiacion
    {
        public bool exito { get; set; }
    }
}