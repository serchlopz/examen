﻿using System;
using System.Web.Mvc;
using EjercicioPropedeutico.Business;
using EjercicioPropedeutico.Etities;
using EjercicioPropedeutico.Models;
using System.Collections.Generic;
using System.Linq;

namespace EjercicioPropedeutico.Controllers
{
    public class ProductoController : Controller
    {
        public ActionResult Index(string busqueda)
        {
            ProductoBusiness ProductoBusiness = new ProductoBusiness();
            List<ProductoModel1> productos = new List<ProductoModel1>();
            var productoBase = ProductoBusiness.ObtenerTodos();
            productos = productoBase.Select(p =>
                     new ProductoModel1()
                     {
                         Id = p.id_producto,
                         IdProveedor = p.id_proveedor,
                         RazonSocialProveedor=p.razon_social,
                         Codigo = p.codigo,
                         Descripcion = p.descripcion,
                         Unidad = p.unidad,
                         Costo = p.costo
                     }).ToList();
            if(!string.IsNullOrWhiteSpace(busqueda))
            {
                productos = productos.Where(p=>p.Descripcion.Contains(busqueda)||
                    p.Codigo.Contains(busqueda)||
                    p.RazonSocialProveedor.Contains(busqueda)).ToList();
            }
            productos = productos.OrderBy(p =>p.RazonSocialProveedor).ToList();
            return View(productos);
        }

        [HttpGet]
        public ActionResult Add()
        {
            ProveedoresBusiness proveedoresBusiness = new ProveedoresBusiness();
            ViewBag.Proveedores = proveedoresBusiness.ObtenerTodos();
            return View();
        }
        [HttpPost]
        public ActionResult Add(ProductoModel1 model)
        {
            ProveedoresBusiness proveedoresBusiness = new ProveedoresBusiness();
            ViewBag.Proveedores = proveedoresBusiness.ObtenerTodos();
            if (model == null)
            {
                throw new ArgumentNullException(nameof(model));
            }
            if (ModelState.IsValid)
            {
                Producto productoAgregar = new Producto()
                {
                    id_producto = model.Id,
                    id_proveedor = model.IdProveedor,
                    codigo = model.Codigo,
                    descripcion = model.Descripcion,
                    unidad = model.Unidad,
                    costo = model.Costo
                };
                new ProductoBusiness().Agregar(productoAgregar);
                ViewBag.Mensaje = "Se guardó correctamete";
                return View(new Models.ProductoModel1());
            }
            return View(model);
        }
        public ActionResult Edit(int id)
        {
            ProductoBusiness productoBusiness = new ProductoBusiness();
            Producto producto = productoBusiness.ObtenerPorId(id);
            ProductoModel1 model = new ProductoModel1()
            {
                Id = producto.id_producto,
                IdProveedor = producto.id_proveedor,
                Codigo = producto.codigo,
                Descripcion = producto.descripcion,
                Unidad = producto.unidad,
                Costo = producto.costo
            };
            return View(model);
        }
        public ActionResult Remove(int Id)
        {
            ProductoBusiness productoBusiness = new ProductoBusiness();
            productoBusiness.EliminarPorId(Id);
            TempData["Mensaje"] = "Eliminado correctamente";//aqui necesito agregar mi eliminacion del elemento
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult Edit(ProductoModel1 model)
        {
            if (ModelState.IsValid)
            {
                Etities.Producto productoAEditar = new Etities.Producto()
                {
                    id_producto = model.Id,
                    id_proveedor = model.IdProveedor,
                    codigo = model.Codigo,
                    descripcion = model.Descripcion,
                    unidad = model.Unidad,
                    costo = model.Costo
                };
                new ProductoBusiness().Edit(productoAEditar);
                TempData["Mensaje"] = "Se guardó correctamete";
                return RedirectToAction("Index");
            }
            return View(model);
        }

    }
}