﻿using System;
using System.Collections.Generic;// list y enumerables
using System.Linq;//where 
using EjercicioPropedeutico.Etities;

namespace EjercicioPropedeutico.DataAcces
{
    public class ProveedoresDAO
    {
        public void Agregar(Proveedores proveedores)
        {
            using (var dbContext = new AppSergioEntities())
            {
                dbContext.Proveedores.Add(proveedores);
                dbContext.SaveChanges();
            }
        }
        public List<Proveedores> ObtenerTodos()
        {
            using (var dbContext = new AppSergioEntities())
            {
                return dbContext.Proveedores.ToList();
            }
        }
        public Proveedores ObtenerPorId(int IdProveedor)
        {
            using (var dbContext = new AppSergioEntities())
            {
                return dbContext.Proveedores.Where(p => p.id_proveedor == IdProveedor).First();//devueleve el primer elemento
            }
        }

        public void ElimiarPorId(int idProveedores)
        {
            using (var dbContext = new AppSergioEntities())
            {
                Proveedores proveedoresEnBD = dbContext.Proveedores.Where(p => p.id_proveedor== idProveedores).FirstOrDefault();
                if (proveedoresEnBD != null)
                    dbContext.Proveedores.Remove(proveedoresEnBD);
                dbContext.SaveChanges();
            }
            
        }

        public void Edit(Proveedores proveedores)
        {
            using (var dbContext = new AppSergioEntities())
            {
                var proveedorEnBD = dbContext.Proveedores.Where(p => proveedores.id_proveedor == p.id_proveedor).FirstOrDefault();
                if (proveedorEnBD != null)
                {
                    proveedorEnBD.codigo = proveedores.codigo;
                    proveedorEnBD.razon_social = proveedores.razon_social;
                    proveedorEnBD.RFC = proveedores.razon_social;
                }
                dbContext.SaveChanges();
            }
        }
    }
}
