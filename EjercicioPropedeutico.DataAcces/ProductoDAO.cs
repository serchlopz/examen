﻿using EjercicioPropedeutico.Etities;
using System;
using System.Collections.Generic;
using System.Linq;


namespace EjercicioPropedeutico.DataAcces
{
    public class ProductoDAO
    {
        public void Agregar(Producto producto)
        {
            using (var dbContext = new AppSergioEntities())
            {
                dbContext.Producto.Add(producto);
                dbContext.SaveChanges();
            }
        }
        public List<vw_all_producto> Obtenertodos()
        {
            using (var dbContext = new AppSergioEntities())
            {
                return dbContext.vw_all_producto.ToList();
            }
        }
        public Producto ObtenerPorId(int idProducto)
        {
            using (var dbContext = new AppSergioEntities())
            {
                return dbContext.Producto.Where(p => p.id_producto == idProducto).First();
            }
        }
        public void EliminarPorId(int idProducto)
        {
            using (var dbContext = new AppSergioEntities())
            {
                Producto productoEnBD = dbContext.Producto.Where(p => p.id_producto == idProducto).FirstOrDefault();
                if (productoEnBD != null)
                    dbContext.Producto.Remove(productoEnBD);
                dbContext.SaveChanges();
            }
        }

        public List<Producto> ObtenerPorProveedor(int idProveedor)
        {
            using (var dbContext = new AppSergioEntities())
            {
                return dbContext.Producto.Where(p => p.id_proveedor == idProveedor).ToList();
            }
        }

        public void Edit(Producto producto)
        {
            using (var dbContext = new AppSergioEntities())
            {
                var productoEnBD = dbContext.Producto.Where(p => producto.id_producto == p.id_producto).FirstOrDefault();
                if (productoEnBD != null)
                {
                    productoEnBD.id_proveedor = producto.id_proveedor;
                    productoEnBD.codigo = producto.codigo;
                    productoEnBD.descripcion = producto.descripcion;
                    productoEnBD.unidad = producto.unidad;
                    productoEnBD.costo = producto.costo;
                }
                dbContext.SaveChanges();
            }
        }

    }
}
