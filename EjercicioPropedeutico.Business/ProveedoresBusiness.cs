﻿using System;
using System.Collections.Generic;
using EjercicioPropedeutico.DataAcces;
using EjercicioPropedeutico.Etities;

namespace EjercicioPropedeutico.Business
{
    public class ProveedoresBusiness
    {
        public void Agregar(Etities.Proveedores proveedores)
        {
            ProveedoresDAO dao = new ProveedoresDAO();
            dao.Agregar(proveedores);
        }

        public List<Proveedores> ObtenerTodos()
        {
            ProveedoresDAO dao = new ProveedoresDAO();
            return dao.ObtenerTodos();
        }

        public Proveedores ObtenerPorId(int Id)
        {
            ProveedoresDAO dao = new ProveedoresDAO();
            return dao.ObtenerPorId(Id);
        }
        public void EliminarPorId(int id, bool forzar)
        {
            ProveedoresDAO dao = new ProveedoresDAO();
            if (id < 0)
            {
                throw new IdNegativoException("El id debe ser positivo");
            }
            if(forzar)
            {
                ProductoBusiness productoBusiness = new ProductoBusiness();
                var productos = productoBusiness.ObtenerPorProveedor(id);
                //ejemplo de un Foreach y abajo un FOR
                foreach(var producto in productos)
                {
                    productoBusiness.EliminarPorId(producto.id_producto);
                }
                /*/
                for(int i=0; i < productos.Count; i++)
                {
                    var producto = productos[i];
                    ///************************************
                    productoBusiness.EliminarPorId(producto.id_producto);
                }
                //*/
            }
            dao.ElimiarPorId(id);
        }
        public void Edit(Proveedores proveedorAEditar)
        {
            ProveedoresDAO dao = new ProveedoresDAO();
            dao.Edit(proveedorAEditar);
        }
    }
}
