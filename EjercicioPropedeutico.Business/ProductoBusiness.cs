﻿using EjercicioPropedeutico.DataAcces;
using System.Collections.Generic;
using EjercicioPropedeutico.Etities;
using System;

namespace EjercicioPropedeutico.Business
{
    public class ProductoBusiness
    {
        public void Agregar(Etities.Producto producto)
        {
            ProductoDAO dao = new ProductoDAO();
            dao.Agregar(producto);
        }
        public List<vw_all_producto> ObtenerTodos()
        {
            ProductoDAO dao = new ProductoDAO();
            return dao.Obtenertodos();
        }

        public Producto ObtenerPorId(int Id)
        {
            ProductoDAO dao = new ProductoDAO();
            return dao.ObtenerPorId(Id);
        }
        public void Edit(Producto productoAEditar)
        {
            ProductoDAO dao = new ProductoDAO();
            dao.Edit(productoAEditar);
        }
        public void EliminarPorId(int id)
        {
            ProductoDAO dao = new ProductoDAO();
            dao.EliminarPorId(id);
        }

        public void Edit(object productoAEditar)
        {
            throw new NotImplementedException();
        }

        public List<Producto> ObtenerPorProveedor(int IdProveedor)
        {
            ProductoDAO dao = new ProductoDAO();
            return dao.ObtenerPorProveedor(IdProveedor);
        }
    }
}
