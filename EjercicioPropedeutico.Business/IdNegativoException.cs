﻿using System;
using System.Runtime.Serialization;

namespace EjercicioPropedeutico.Business
{
    [Serializable]
    public class IdNegativoException : Exception
    {
        public IdNegativoException()
        {
        }

        public IdNegativoException(string message) : base(message)
        {
        }

        public IdNegativoException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected IdNegativoException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}